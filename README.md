Hello Duncan,   
Thank you for taking the time to go through my profile, I really appreciate it.    

#### Projessional Summary:     
I did my **Bachelors in Electrical and Electronics** engineering in 2014.    
I was really interested in **Embedded systems** and did some advanced courses in that area after Graduation.         

Later I worked with a Startup in India, Designing IoT systems for energy management applications. I worked there for one year+.     

Currently I am towards the end of **Masters in Engineering (Mechatronics)** at the University of Malaya in Malaysia.   
I am also in the last term of **Udacity Self driving-car Nanodegree Program**.      
The Udacity nanodegree I will be done in a week or two, and the M.Eng in 2 months. **So right now I am strarting to look for job opportunities**.     

#### Career Goals:    
My goal is to work as a **software developer** in **Automotive** or related Industries, particularly in ADAS/Algorithm development/ML.    

I realize **R&D** jobs demands for professionals with proven experience, and I might not have much to show for in that regard.    
I would like to hear your suggestions on how I could best plan a career tarjectory that would help me achieve my goals.

Thank you

---

# Personal_profile
Linkedin account : https://www.linkedin.com/in/muhsinm/    
Resume updated Recently : [link](https://drive.google.com/open?id=1K86r-3EKBzbOTJNu88GfvBTydXi9BBxp)    
